//
//  ListMuseumController.h
//  MuseeSainteCroix
//
//  Created by local192 on 04/06/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "DBManager.h"
#import "EstimoteStatus.h"

@interface ListMuseumController : UIViewController

/*---DataBase---*/
@property (nonatomic) DBManager *dbManager;
@property (nonatomic) NSArray *arrMuseums;

/*---Beacon Storage----*/
@property (nonatomic) ESTDeviceManager *deviceManager;
@property (nonatomic) ESTDeviceFilterLocationBeacon *filterBeaconDevice;
@property (nonatomic) NSArray <ESTDeviceLocationBeacon *> *beaconDeviceArray;

@property (nonatomic) ESTDeviceLocationBeacon *beaconDeviceMuseum;

@property (nonatomic) NSString *beaconIDMuseum;

@property (nonatomic) NSString * nameMuseum;
@property (nonatomic) NSString * typeBeacon;
@property (nonatomic) NSString * descMuseum;
@property (nonatomic) NSDictionary * addressMuseumJSON;
@property (nonatomic) float latitudeMuseum;
@property (nonatomic) float longitudeMuseum;

/*-------Others--------*/
@property (nonatomic) EstimoteStatus estimoteStatus;
@property (nonatomic) NSString *languageSystem;

@end
