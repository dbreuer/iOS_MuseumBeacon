//
//  ListMuseumController.m
//  MuseeSainteCroix
//
//  Created by local192 on 04/06/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "ListMuseumController.h"
#import "MenuController.h"

@interface ListMuseumController ()<ESTDeviceConnectableDelegate, ESTDeviceManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tvListMuseum;
@property (weak, nonatomic) IBOutlet UIButton *btnNewMuseum;
@property (weak, nonatomic) IBOutlet UIView *viewStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivStatus;
@property (weak, nonatomic) IBOutlet UITextView *tvStatus;

- (IBAction)btnNewMuseum:(UIButton *)sender;

@end

@implementation ListMuseumController

/*==========================================*/
/*==== Methods Configuration Controller ====*/
/*==========================================*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonBack", nil)]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    
    
    self.title = [NSString stringWithFormat: NSLocalizedString(@"TitleListMuseumController", nil)];
    [self.btnNewMuseum setTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonListMuseum", nil)] forState:UIControlStateNormal];
    
    NSLog(@"✅SUCCESS : Welcome to Museum's List!");
    
    [self configureTools];
    
    self.estimoteStatus = StatusDisconnected;
    [self updateStateView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateListMuseum];
}

- (void)configureTools {
    
    NSLog(@"✴️STATE : Initializing Device Manager.....");
    self.deviceManager = [[ESTDeviceManager alloc] init];
    self.deviceManager.delegate = self;
    
    NSLog(@"✴️STATE : Initializing Museum's TableView.....");
    self.tvListMuseum.delegate = self;
    self.tvListMuseum.dataSource = self;
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"Multi_Museums.db"];
    
    self.languageSystem = [[NSLocale preferredLanguages] firstObject];
    NSLog(@"Language System : %@", self.languageSystem);
}

- (void) updateStateView {
    switch(self.estimoteStatus)
    {
        case (StatusDisconnected):
            self.viewStatus.hidden = YES;
            self.tvListMuseum.hidden = NO;
            self.btnNewMuseum.hidden = NO;
            break;
            
        case (StatusResearching):
            self.viewStatus.hidden = NO;
            self.tvListMuseum.hidden = YES;
            self.btnNewMuseum.hidden = YES;
            self.tvStatus.text = [NSString stringWithFormat: NSLocalizedString(@"StateResearching", nil)];
            [self.aivStatus startAnimating];
            break;
        case (StatusConnecting):
            self.tvStatus.text = [NSString stringWithFormat: NSLocalizedString(@"StateConnecting", nil)];;
            break;
            
        case (StatusConnected):
            self.tvStatus.text = [NSString stringWithFormat: NSLocalizedString(@"StateSaving", nil)];;
            break;
            
        case (StatusRead):
            self.viewStatus.hidden = YES;
            self.tvListMuseum.hidden = NO;
            self.btnNewMuseum.hidden = NO;
            break;
            
        default:
            break;
    }
}

/*=========================*/
/*==== Methods Beacons ====*/
/*=========================*/

- (IBAction)btnNewMuseum:(UIButton *)sender {
    self.estimoteStatus = StatusResearching;
    [self updateStateView];
    
    NSLog(@"✴️STATE : Searching Museum's Beacon.....");
    
    self.filterBeaconDevice = [[ESTDeviceFilterLocationBeacon alloc] init];
    [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
}


- (void)deviceManager:(ESTDeviceManager *)manager didDiscoverDevices:(NSArray<ESTDevice *> *)devices {
    if (devices.count > 0) {
        [self.deviceManager stopDeviceDiscovery];
        
        self.beaconDeviceArray = (NSArray <ESTDeviceLocationBeacon *> *)devices;
        self.beaconDeviceMuseum = self.beaconDeviceArray[0];
        self.beaconIDMuseum = self.beaconDeviceMuseum.identifier;
        
        NSLog(@"Beacon museum found : %@", self.beaconIDMuseum);
        
        if(![self verifyBeaconInDB]) {
            self.estimoteStatus = StatusConnecting;
            [self updateStateView];
            
            NSLog(@"✴️STATE : Connecting.....");
            self.beaconDeviceMuseum.delegate = self;
            [self.beaconDeviceMuseum connectForStorageRead];
        }
        else {
            self.estimoteStatus = StatusRead;
            [self updateStateView];
        }
    }
}

- (void)estDeviceConnectionDidSucceed:(ESTDeviceConnectable *)device {
    NSLog(@"✅SUCCESS : Beacon connected!");
    self.estimoteStatus = StatusConnected;
    [self updateStateView];
    NSLog(@"✴️STATE : Reading storage.....");
    [self.beaconDeviceMuseum.storage readStorageDictionaryWithCompletion:^(NSDictionary * _Nullable value, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"✅SUCCESS : Museum's informations recovered!");
            self.estimoteStatus = StatusRead;
            [self updateStateView];
            NSDictionary *parsedJSON = [self parseStringtoJSON: value];
            
            if([(self.typeBeacon = [parsedJSON objectForKey:@"additionalType"]) isEqualToString: @"Museum"])
            {
            
                self.nameMuseum = [parsedJSON objectForKey:@"name"];
                self.descMuseum = [self parseJSONArrayToString:[parsedJSON objectForKey:@"additionalProperty"]];
                self.addressMuseumJSON = [parsedJSON objectForKey:@"address"];
                self.latitudeMuseum = [[[parsedJSON objectForKey:@"geo"] objectForKey:@"latitude"] floatValue];
                self.longitudeMuseum = [[[parsedJSON objectForKey:@"geo"] objectForKey:@"longitude"] floatValue];
                
                NSLog(@"✴️STATE : Recording in database.....");
                NSString *query = [NSString stringWithFormat:@"insert into Address values(null, '%@', '%@', '%@', '%@', '%@', '%f', '%f')", [self.addressMuseumJSON objectForKey:@"addressCountry"], [self.addressMuseumJSON objectForKey:@"addressRegion"], [self.addressMuseumJSON objectForKey:@"addressLocality"], [self.addressMuseumJSON objectForKey:@"postalCode"], [self.addressMuseumJSON objectForKey:@"streetAddress"], self.latitudeMuseum, self.longitudeMuseum];
                [self saveData:query];
                
                query = @"SELECT MAX(id) FROM Address";
                NSArray *lastIDAddress = [self loadMuseums:query];
                query = [NSString stringWithFormat:@"insert into Museum values(null, '%@', '%@', '%@', '%@')", self.beaconDeviceMuseum.identifier , self.nameMuseum, self.descMuseum, lastIDAddress[0]];
                [self saveData:query];
                
                NSLog(@"✅SUCCESS : Museum added!");
                [self updateListMuseum];
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleMuseumDBSUCCESS", nil)]
                                                                               message:[NSString stringWithFormat: NSLocalizedString(@"AlertMessageMuseumDBSUCCESS", nil)]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:nil];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                NSLog(@"❌ERROR : Museum not added! The beacon is not museum type!");
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleMuseumERROR", nil)]
                                                                               message:[NSString stringWithFormat: NSLocalizedString(@"AlertMessageMuseumERROR", nil)]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:nil];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            NSLog(@"✴️STATE : Disconnecting.....");
            [self.beaconDeviceMuseum disconnect];
        }
        else {
            NSLog(@"❌ERROR : Read failed : %@", error.localizedDescription);
        }
    }];
}

- (NSDictionary *) parseStringtoJSON : (NSDictionary *) value {
    NSString *s = [value objectForKey:@"json"];
    NSData *jsonData = [s dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (!error) {
        if ([jsonObject isKindOfClass:[NSDictionary class]])
        {
            return (NSDictionary *)jsonObject;
        }
    }
    NSLog(@"❌ERROR : Error parsing JSON: %@", error);
    return nil;
}

- (NSString *) parseJSONArrayToString : (NSArray *) json {
    NSError *error;
    NSData *myDataAsJson = [NSJSONSerialization dataWithJSONObject:json
                                                           options:NSJSONWritingSortedKeys
                                                             error:&error];
    
    if (!error)
    {
        NSString *jsonString = [[NSString alloc] initWithData:myDataAsJson encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    NSLog(@"❌ERROR : Error parsing JSON to String: %@", error);
    return nil;
}

/*==========================*/
/*==== Methods Database ====*/
/*==========================*/

-(BOOL)verifyBeaconInDB {
    for(NSArray* indexRow in self.arrMuseums)
    {
        if([indexRow[1] isEqualToString:self.beaconDeviceMuseum.identifier])
        {
            NSLog(@"ℹ️INFORMATION : The museum has already been visited.");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleMuseumDB", nil)]
                                                                           message:[NSString stringWithFormat: NSLocalizedString(@"AlertMessageMuseumDB", nil)]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:nil];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return YES;
        }
    }
    NSLog(@"ℹ️INFORMATION : The museum has never been visited");
    return NO;;
}

-(void)saveData : (NSString *) query{
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"✅SUCCESS : Query was executed successfully : Row affected!");
    }
    else{
        NSLog(@"❌ERROR : Could not execute the query.");
    }
    
}

-(NSArray *)loadMuseums : (NSString *) query{
    // Get the results.
    if (self.arrMuseums != nil) {
        self.arrMuseums = nil;
    }
    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

/*===========================*/
/*==== Methods TableView ====*/
/*===========================*/

- (void) updateListMuseum {
    NSString *query = @"select * from Museum order by Museum.Name";
    self.arrMuseums = [self loadMuseums: query];
    NSLog(@"ℹ️INFORMATION : %lu museum(s) in register", [self.arrMuseums count]);
    NSLog(@"✴️STATE : Updating Museum's TableView.....");
    [self.tvListMuseum reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"CellMuseum";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0];
    cell.textLabel.text = self.arrMuseums[indexPath.row][2];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrMuseums count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.beaconIDMuseum = self.arrMuseums[indexPath.row][1];
    NSLog(@"✅SUCCESS : Museum selected : %@", self.beaconIDMuseum);
    [self performSegueWithIdentifier:@"goMenu" sender:self];
}

/*====================*/
/*==== Navigation ====*/
/*====================*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goMenu"]) {
        MenuController *destinationVC = (MenuController *)segue.destinationViewController;
        destinationVC.beaconIDMuseum = self.beaconIDMuseum;
    }
}

@end
