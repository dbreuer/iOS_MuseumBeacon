//
//  MapController.h
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapController : UIViewController

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * address;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;

@end
