//
//  DBManager.m
//  BeaconReadTest
//
//  Created by local192 on 25/04/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"

@implementation DBManager

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    NSLog(@"✴️STATE : Initializing database.....");
    self = [super init];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        self.databaseFilename = dbFilename;
        self.databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
        
        [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}

-(void)copyDatabaseIntoDocumentsDirectory{
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.databasePath]) {
        NSLog(@"ℹ️INFORMATION : Database isn't in documents directory!");
        NSLog(@"✴️STATE : Copying the database file into the documents directory.....");
        
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:self.databasePath error:&error];
        
        if (!error){
            NSLog(@"✅SUCCESS : Database copied!");
        }
        else {
            NSLog(@"❌ERROR : Copying failed : %@", [error localizedDescription]);
        }
    }
}

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Open the database.
    NSLog(@"✴️STATE : Opening database.....");
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        NSLog(@"✅SUCCESS : Database opened!");
        
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        NSLog(@"✴️STATE : Preparing statement result.....");
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            NSLog(@"✅SUCCESS : Statement result prepared!");
            // Check if the query is non-executable.
            if (!queryExecutable){
                // In this case data must be loaded from the database.
                NSLog(@"✴️STATE : Executing query : Read.....");
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                    
                    NSLog(@"✅SUCCESS : Query executed!");
                }
            }
            else {
                // This is the case of an executable query (insert, update, ...).
                NSLog(@"✴️STATE : Executing query : Edit.....");
                // Execute the query.
                if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                    // Keep the affected rows.
                    NSLog(@"✅SUCCESS : Query executed!");
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    NSLog(@"❌ERROR : Query not executed : %s", sqlite3_errmsg(sqlite3Database));
                }
            }
        }
        else {
            // In the database cannot be opened then show the error message on the debugger.
            NSLog(@"❌ERROR : Statement result not prepared : %s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
        
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
    NSLog(@"✅SUCCESS : Database closed!");
}

-(NSArray *)loadDataFromDB:(NSString *)query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

-(void)executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

@end
