//
//  ViewController.m
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *ivLogo;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivLoading;
@property (weak, nonatomic) IBOutlet UILabel *lbLoading;

@end


@implementation ViewController

/*==========================================*/
/*==== Methods Configuration Controller ====*/
/*==========================================*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"✴️STATE : Opening application.....");
    
    self.lbLoading.text = [NSString stringWithFormat: NSLocalizedString(@"StateLoading", nil)];
    [self.aivLoading startAnimating];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"✴️STATE : Go Menu.....");
        [self performSegueWithIdentifier:@"goMuseumList" sender:self];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
