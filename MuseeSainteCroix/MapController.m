//
//  MapController.m
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "MapController.h"

@interface MapController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mvAddMuseum;
@property (weak, nonatomic) IBOutlet UITextView *tvAddMuseum;
@property (weak, nonatomic) IBOutlet UIButton *btnPlan;

- (IBAction)btnPlan:(UIButton *)sender;
@end


@implementation MapController

/*==========================================*/
/*==== Methods Configuration Controller ====*/
/*==========================================*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat: NSLocalizedString(@"TitleMapController", nil)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonBack", nil)]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    // Do any additional setup after loading the view.
    
    self.mvAddMuseum.delegate = self;
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = coord;
    point.title = self.name;
    [self.mvAddMuseum addAnnotation:point];
    
    self.tvAddMuseum.text = self.address;
    
    self.btnPlan.layer.cornerRadius = 5;
    self.btnPlan.clipsToBounds = YES;
    [self.btnPlan setTitle:[NSString stringWithFormat: NSLocalizedString(@"thW-U0-Kbs.normalTitle", nil)] forState:UIControlStateNormal];
    
    NSLog(@"✅SUCCESS : Welcome to A Propos!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnPlan:(UIButton *)sender {
    NSString *customURL = @"http://maps.apple.com/?q=Musee+Sainte+Croix+Poitiers&t=s";
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleMap", nil)]
                                                                       message:[NSString stringWithFormat: NSLocalizedString(@"AlertMessageMap", nil)]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
@end
