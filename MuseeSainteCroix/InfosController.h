//
//  InfosController.h
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "DBManager.h"
#import "EstimoteStatus.h"

@interface InfosController : UIViewController

/*---Base de données---*/
@property (nonatomic) DBManager *dbManager;
@property (nonatomic) NSArray *arrLevel;
@property (nonatomic) NSArray *arrArtwork;
@property (nonatomic) NSArray *arrSugg;
@property (nonatomic) int indexReadRowDB;

/*---Beacon Storage----*/
@property (nonatomic) ESTDeviceManager *deviceManager;
@property (nonatomic) ESTDeviceFilterLocationBeacon *filterBeaconDevice;
@property (nonatomic) NSArray *beaconDeviceArray;

@property (nonatomic) ESTDeviceLocationBeacon *beaconDevice;

@property (nonatomic) NSString * beaconIDMuseum;
@property (nonatomic) NSString * currentBeaconID;
@property (nonatomic) NSString * typeBeacon;
@property (nonatomic) NSString * nameLevel;
@property (nonatomic) NSString * descLevel;
@property (nonatomic) NSString * mapLevel;

@property (nonatomic) NSString * nameArtwork;
@property (nonatomic) NSString * descArtwork;
@property (nonatomic) float latitudeArtwork;
@property (nonatomic) float longitudeArtwork;
@property (nonatomic) NSString * dateArtwork;
@property (nonatomic) NSString * beaconIDLevelArtwork;
@property (nonatomic) NSArray *artworkSugg;
@property (nonatomic) NSString *artworkSuggName;
@property (nonatomic) NSString *artworkSuggMap;
@property (nonatomic) float artworkSuggLatitude;
@property (nonatomic) float artworkSuggLongitude;

/*-------Others--------*/
@property (nonatomic) EstimoteStatus estimoteStatus;
@property (nonatomic) BOOL infosMuseum;
@property (nonatomic) NSString *languageSystem;

@end
