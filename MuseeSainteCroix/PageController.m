//
//  PageController.m
//  MuseeSainteCroix
//
//  Created by local192 on 07/06/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "PageController.h"

@interface PageController ()

@end

@implementation PageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self updateViewAtIndex: self.index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateViewAtIndex: (NSUInteger)index {
    switch (index)
    {
        case 0:
            self.lbManual.text = [NSString stringWithFormat: NSLocalizedString(@"TitleManual0", nil)];
            self.tvManual.text = [NSString stringWithFormat: NSLocalizedString(@"TextManual0", nil)];
            break;
            
        case 1:
            self.lbManual.text = [NSString stringWithFormat: NSLocalizedString(@"TitleManual1", nil)];
            self.tvManual.text = [NSString stringWithFormat: NSLocalizedString(@"TextManual1", nil)];
            break;
            
        case 2:
            self.lbManual.text = [NSString stringWithFormat: NSLocalizedString(@"TitleManual2", nil)];
            self.tvManual.text = [NSString stringWithFormat: NSLocalizedString(@"TextManual2", nil)];
            break;
            
        case 3:
            self.lbManual.text = [NSString stringWithFormat: NSLocalizedString(@"TitleManual3", nil)];
            self.tvManual.text = [NSString stringWithFormat: NSLocalizedString(@"TextManual3", nil)];
            break;
            
        case 4:
            self.lbManual.text = [NSString stringWithFormat: NSLocalizedString(@"TitleManual4", nil)];
            self.tvManual.text = [NSString stringWithFormat: NSLocalizedString(@"TextManual4", nil)];
            break;
            
        default:
            break;
    }
}

@end
