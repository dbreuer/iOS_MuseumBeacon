//
//  MenuController.m
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "MenuController.h"
#import "InfosController.h"
#import "ManualController.h"
#import "AboutController.h"

@interface MenuController ()

@property (weak, nonatomic) IBOutlet UIImageView *ivLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnVisit;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;
@property (weak, nonatomic) IBOutlet UIButton *btnMuseum;

@property (weak, nonatomic) IBOutlet UIView *viewStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivStatus;
@property (weak, nonatomic) IBOutlet UITextView *tvStatus;

- (IBAction)btnVisit:(UIButton *)sender;
- (IBAction)btnManual:(UIButton *)sender;
- (IBAction)btnMuseum:(UIButton *)sender;

@end


@implementation MenuController

/*==========================================*/
/*==== Methods Configuration Controller ====*/
/*==========================================*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"";
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonBack", nil)]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    
    [self.btnVisit setTitle: [NSString stringWithFormat: NSLocalizedString(@"ButtonMenuVisit", nil)]forState:UIControlStateNormal];
    [self.btnManual setTitle: [NSString stringWithFormat: NSLocalizedString(@"ButtonMenuManual", nil)]forState:UIControlStateNormal];
    [self.btnMuseum setTitle: [NSString stringWithFormat: NSLocalizedString(@"ButtonMenuMuseum", nil)]forState:UIControlStateNormal];
    
    NSLog(@"✅SUCCESS : Welcome to Menu!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*==============================*/
/*==== Methods Buttons Menu ====*/
/*==============================*/


- (IBAction)btnVisit:(UIButton *)sender {
    [self performSegueWithIdentifier:@"goVisit" sender:self];
}

- (IBAction)btnManual:(UIButton *)sender {
    [self performSegueWithIdentifier:@"goManual" sender:self];
}

- (IBAction)btnMuseum:(UIButton *)sender {
    [self performSegueWithIdentifier:@"goMuseum" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goVisit"]) {
        InfosController *destinationVC = (InfosController *)segue.destinationViewController;
        destinationVC.beaconIDMuseum = self.beaconIDMuseum;
        destinationVC.infosMuseum = NO;
    }
    else if ([segue.identifier isEqualToString:@"goMuseum"]) {
        AboutController *destinationVC = (AboutController *)segue.destinationViewController;
        destinationVC.beaconIDMuseum = self.beaconIDMuseum;
    }
}

@end
