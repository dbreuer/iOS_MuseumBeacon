//
//  SavoirPlusController.h
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "DBManager.h"
#import "EstimoteStatus.h"

@interface AboutController : UIViewController

/*---Base de données---*/
@property (nonatomic) DBManager *dbManager;
@property (nonatomic) NSArray *arrMuseums;
@property (nonatomic) NSArray *arrArtwork;

/*---Beacon Storage----*/
@property (nonatomic) NSString * beaconIDMuseum;
@property (nonatomic) NSString * beaconIDArtwork;

@property (nonatomic) NSString * nameMuseum;
@property (nonatomic) NSString * descMuseum;
@property (nonatomic) NSString * addressMuseumString;
@property (nonatomic) float latitudeMuseum;
@property (nonatomic) float longitudeMuseum;

/*-------Others--------*/
@property (nonatomic) EstimoteStatus estimoteStatus;
@property (nonatomic) NSString *languageSystem;

@end
