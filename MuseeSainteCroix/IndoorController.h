//
//  IndoorController.h
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndoorController : UIViewController

@property (nonatomic) NSString * nameArtwork;
@property (nonatomic) NSString * mapArtwork;
@property (nonatomic) float latitudeArtwork;
@property (nonatomic) float longitudeArtwork;

@end
