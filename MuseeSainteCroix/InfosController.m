//
//  InfosController.m
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "InfosController.h"
#import "IndoorController.h"

@interface InfosController ()<ESTDeviceConnectableDelegate, ESTDeviceManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *viewDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UITextView *tvDescriptionArtwork;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;

@property (weak, nonatomic) IBOutlet UIView *viewArtwork;
@property (weak, nonatomic) IBOutlet UILabel *lbArtwork;
@property (weak, nonatomic) IBOutlet UITableView *tvArtwork;

@property (weak, nonatomic) IBOutlet UIView *viewStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivStatus;
@property (weak, nonatomic) IBOutlet UITextView *tvStatus;

- (IBAction)btnLocation:(UIButton *)sender;

@end

@implementation InfosController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.nameLevel != nil)
    {
        self.title = self.nameLevel;
    }
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonBack", nil)]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    // Do any additional setup after loading the view.
    
    [self.lbDescription setText:[NSString stringWithFormat: NSLocalizedString(@"LabelArtworkDescription", nil)]];
    [self.btnLocation setTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonArtworkPosition", nil)] forState:UIControlStateNormal];
    [self.lbArtwork setText:[NSString stringWithFormat: NSLocalizedString(@"LabelArtworkSugg", nil)]];
    
    NSLog(@"✅SUCCESS : Welcome to Infos!");
    
    self.arrLevel = [[NSArray alloc] init];
    self.arrArtwork = [[NSArray alloc] init];
    self.arrSugg = [[NSArray alloc] init];
    
    self.deviceManager = [[ESTDeviceManager alloc] init];
    self.deviceManager.delegate = self;
    
    self.tvArtwork.delegate = self;
    self.tvArtwork.dataSource = self;
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"Multi_Museums.db"];
    
    self.languageSystem = [[NSLocale preferredLanguages] firstObject];
    
    if(self.infosMuseum)
    {
        NSString *query = @"SELECT * FROM Artwork";
        self.arrArtwork = [self loadData:query forDatabase:self.arrArtwork];
        [self readArtworkFromDBAtIndex:self.indexReadRowDB];
    }
    else {
        NSLog(@"✴️STATE : Searching Level's|Artowrk's Beacon.....");
        
        self.estimoteStatus = StatusResearching;
        [self updateView];
        
        self.filterBeaconDevice = [[ESTDeviceFilterLocationBeacon alloc] init];
        [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tvArtwork reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)deviceManager:(ESTDeviceManager *)manager didDiscoverDevices:(NSArray<ESTDevice *> *)devices {
    if (devices.count > 0) {
        self.beaconDeviceArray = (NSArray <ESTDeviceLocationBeacon *> *)devices;
        self.beaconDevice = self.beaconDeviceArray[0];
        
        
        if(![self.currentBeaconID isEqualToString:self.beaconDevice.identifier])
        {
            self.currentBeaconID = self.beaconDevice.identifier;
        
            NSLog(@"✅SUCCESS : Beacon selected : %@",self.currentBeaconID);
            self.beaconDevice.delegate = self;
            
            [self.deviceManager stopDeviceDiscovery];
            
            NSString *query = @"SELECT * FROM Level";
            self.arrLevel = [self loadData:query forDatabase:self.arrLevel];
            NSLog(@"✅SUCCESS : Database read! Number of levels in DB : %lu", [self.arrLevel count]);
            query = @"SELECT * FROM Artwork";
            self.arrArtwork = [self loadData:query forDatabase:self.arrArtwork];
            NSLog(@"✅SUCCESS : Database read! Number of artworks in DB : %lu", [self.arrArtwork count]);
            
            if((self.indexReadRowDB = [self verifyBeaconInDB:self.arrLevel forBeacon:self.currentBeaconID]) >= 0)
            {
                NSLog(@"ℹ️INFORMATION : Reading database for Level");
                [self readLevelFromDBAtIndex:self.indexReadRowDB];
            }
            else if((self.indexReadRowDB = [self verifyBeaconInDB:self.arrArtwork forBeacon:self.currentBeaconID]) >= 0)
            {
                NSLog(@"ℹ️INFORMATION : Reading database for Artwork");
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleConfirmRead", nil)]
                                                                               message:[NSString stringWithFormat:@"\"%@", [self.arrArtwork[self.indexReadRowDB][2] stringByAppendingString: NSLocalizedString(@"AlertMessageConfirmRead", nil)]]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * _Nonnull action)
                                                {
                                                    [self readArtworkFromDBAtIndex:self.indexReadRowDB];
                                                }];
                
                [alert addAction:confirmAction];
                
                UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action)
                                               {
                                                   [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
                                               }];
                [alert addAction:cancelAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                NSLog(@"ℹ️INFORMATION : Reading from Beacon!");
                [self readFromBeacon];
            }
        }
    }
}

- (void) readFromBeacon {
    NSLog(@"✴️STATE : Connecting.....");
    self.estimoteStatus = StatusConnecting;
    [self updateView];
    
    [self.beaconDevice connectForStorageRead];
};

- (void) readLevelFromDBAtIndex : (NSUInteger) indexRowDB {
    
    NSArray *parsedJSON = [self parseStringtoJSONArray:self.arrLevel[indexRowDB][2]];
    for (NSDictionary * indexNames in parsedJSON) {
        if([[[indexNames objectForKey:@"value"] lowercaseString] hasPrefix: [self.languageSystem substringToIndex:2]]) {
            self.nameLevel = [indexNames objectForKey:@"name"];
        }
    }
    
    self.mapLevel = self.arrLevel[indexRowDB][3];
    
    self.title = self.nameLevel;
    
    self.estimoteStatus = StatusResearching;
    [self updateView];
    
    [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
};

- (void) readArtworkFromDBAtIndex : (NSUInteger) indexRowDB {
    self.nameArtwork = self.arrArtwork[indexRowDB][2];
    
    NSArray *parsedJSON = [self parseStringtoJSONArray:self.arrArtwork[indexRowDB][3]];
    for (NSDictionary * indexDescriptions in parsedJSON) {
        if([[[indexDescriptions objectForKey:@"name"] lowercaseString] hasPrefix: [self.languageSystem substringToIndex:2]]) {
            self.descArtwork = [indexDescriptions objectForKey:@"description"];
        }
    }
    
    self.latitudeArtwork = [self.arrArtwork[indexRowDB][4] floatValue];
    self.longitudeArtwork = [self.arrArtwork[indexRowDB][5] floatValue];
    self.dateArtwork = self.arrArtwork[indexRowDB][6];
    
    NSString *query = [[@"select * from Level where BeaconID = '" stringByAppendingString:self.arrArtwork[indexRowDB][7]] stringByAppendingString:@"'"];
    self.arrLevel = [self loadData: query forDatabase:self.arrLevel];
    for (NSDictionary * indexNames in [self parseStringtoJSONArray:self.arrLevel[0][2]]) {
        if([[[indexNames objectForKey:@"value"] lowercaseString] hasPrefix: [self.languageSystem substringToIndex:2]]) {
            self.nameLevel = [indexNames objectForKey:@"name"];
        }
    }
    self.mapLevel = self.arrLevel[0][3];
    
    self.title = self.nameArtwork;
    self.tvDescriptionArtwork.text = self.descArtwork;
    
    query = [[@"select * from Suggestion where BeaconIDArtwork = '" stringByAppendingString:self.currentBeaconID] stringByAppendingString:@"'"];
    self.arrSugg = [self loadData: query forDatabase:self.arrSugg];
    
    [self.tvArtwork reloadData];
    
    self.estimoteStatus = StatusRead;
    [self updateView];
    
    if(!self.infosMuseum) {
        [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
    }
}

- (void)estDeviceConnectionDidSucceed:(ESTDeviceConnectable *)device {
    NSLog(@"✅SUCCESS : Beacon connected!");
    self.estimoteStatus = StatusConnected;
    [self updateView];
    
    NSLog(@"✴️STATE : Reading storage.....");
    [self.beaconDevice.storage readStorageDictionaryWithCompletion:^(NSDictionary * _Nullable value, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"✅SUCCESS : Storage read!");
            
            NSDictionary * parsedJSON = [self parseStringtoJSONDictionary: value];
            
            if([(self.typeBeacon = [parsedJSON objectForKey:@"additionalType"]) isEqualToString: @"Level"]) {
                [self readLevelFromBeacon:parsedJSON];
            }
            else if([(self.typeBeacon = [parsedJSON objectForKey:@"additionalType"]) isEqualToString: @"Artwork"]) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleConfirmRead", nil)]
                                                                               message:[NSString stringWithFormat:@"\"%@", [[parsedJSON objectForKey:@"name"] stringByAppendingString: NSLocalizedString(@"AlertMessageConfirmRead", nil)]]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * _Nonnull action)
                                                {
                                                    [self readArtworkFromBeacon:parsedJSON];
                                                }];
                
                [alert addAction:confirmAction];
                
                UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action)
                                               {
                                                   [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
                                               }];
                [alert addAction:cancelAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                NSLog(@"❌ERROR : Beacon not added! The beacon is not level or artwork type!");
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: NSLocalizedString(@"AlertTitleBeaconERROR", nil)]
                                                                               message:[NSString stringWithFormat: NSLocalizedString(@"AlertMessageBeaconERROR", nil)]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * _Nonnull action)
                                                {
                                                    self.estimoteStatus = StatusResearching;
                                                    [self updateView];
                                                    [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
                                                }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else {
            NSLog(@"❌ERROR : Read failed : %@", error.localizedDescription);
        }
        NSLog(@"✴️STATE : Disconnecting.....");
        self.estimoteStatus = StatusDisconnected;
        [self.beaconDevice disconnect];
        [self.deviceManager startDeviceDiscoveryWithFilter: self.filterBeaconDevice];
    }];
}

- (void) readLevelFromBeacon : (NSDictionary *) value {
    self.beaconIDLevelArtwork = self.currentBeaconID;
    for(NSDictionary *indexDescription in [value objectForKey:@"additionalProperty"])
    {
        if([[[indexDescription objectForKey:@"value"] lowercaseString] isEqualToString:[self.languageSystem substringToIndex:2]])
        {
            self.nameLevel = [indexDescription objectForKey:@"name"];
        }
    }
    self.mapLevel = [[value objectForKey:@"hasMap"] objectForKey:@"name"];
    
    self.title = self.nameLevel;
    
    self.nameLevel = [self parseJSONArrayToString:[value objectForKey:@"additionalProperty"]];
    
    self.estimoteStatus = StatusResearching;
    [self updateView];
    
    NSLog(@"✴️STATE : Recording in database.....");
    NSString *query = [NSString stringWithFormat:@"insert into Level values(null, '%@', '%@', '%@', '%@')", self.beaconDevice.identifier, self.nameLevel, self.mapLevel, self.beaconIDMuseum];
    
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"✅SUCCESS : Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"❌ERROR : Could not execute the query.");
    }
}

- (void) readArtworkFromBeacon : (NSDictionary *) value {
    self.nameArtwork = [value objectForKey:@"name"];
    for(NSDictionary *indexDescription in [value objectForKey:@"about"])
    {
        if([[[indexDescription objectForKey:@"name"] lowercaseString] isEqualToString: [self.languageSystem substringToIndex:2]])
        {
            self.descArtwork = [indexDescription objectForKey:@"description"];
        }
    }
    self.latitudeArtwork = [[[[value objectForKey:@"contentLocation"] objectForKey:@"geo"] objectForKey:@"latitude"] floatValue];
    self.longitudeArtwork = [[[[value objectForKey:@"contentLocation"] objectForKey:@"geo"] objectForKey:@"longitude"] floatValue];
    
    self.title = self.nameArtwork;
    self.tvDescriptionArtwork.text = self.descArtwork;
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd k:mm:ss"];
    self.dateArtwork = [dateFormatter stringFromDate:currDate];
    
    self.descArtwork = [self parseJSONArrayToString:[value objectForKey:@"about"]];
    
    NSLog(@"✴️STATE : Recording in database.....");
    NSString *query = [NSString stringWithFormat:@"insert into Artwork values(null, '%@', '%@', '%@', '%f', '%f', '%@', '%@')", self.currentBeaconID, self.nameArtwork, self.descArtwork, self.latitudeArtwork, self.longitudeArtwork, self.dateArtwork, self.beaconIDLevelArtwork];
    
    [self.dbManager executeQuery:query];
    
    self.artworkSugg = [[value objectForKey:@"contentLocation"] objectForKey:@"geospatiallyDisjoint"];
    NSLog(@"%@", self.artworkSugg);
    
    for (NSDictionary * indexSugg in self.artworkSugg)
    {
        self.artworkSuggName = [indexSugg objectForKey:@"name"];
        self.artworkSuggMap = [[[indexSugg objectForKey:@"containedInPlace"] objectForKey:@"hasMap"] objectForKey:@"name"];
        self.artworkSuggLatitude = [[[indexSugg objectForKey:@"geo"] objectForKey:@"latitude"] floatValue];
        self.artworkSuggLongitude = [[[indexSugg objectForKey:@"geo"] objectForKey:@"longitude"] floatValue];
        
        query = [NSString stringWithFormat:@"insert into Suggestion values(null, '%@', '%@', '%f', '%f', '%@')", self.beaconDevice.identifier, self.artworkSuggName, self.artworkSuggLatitude , self.artworkSuggLongitude, self.artworkSuggMap];
        
        [self.dbManager executeQuery:query];
    }
    
    query = [[@"select * from Suggestion where BeaconIDArtwork = '" stringByAppendingString:self.currentBeaconID] stringByAppendingString:@"'"];
    self.arrSugg = [self loadData: query forDatabase:self.arrSugg];
    [self.tvArtwork reloadData];
    
    self.estimoteStatus = StatusRead;
    [self updateView];
}

- (NSDictionary *) parseStringtoJSONDictionary : (NSDictionary *) value {
    NSString *s = [value objectForKey:@"json"];
    NSData *jsonData = [s dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (!error) {
        if ([jsonObject isKindOfClass:[NSDictionary class]])
        {
            return (NSDictionary *)jsonObject;
        }
    }
    NSLog(@"❌ERROR : Error parsing String to JSON: %@", error);
    return nil;
}

- (NSString *) parseJSONArrayToString : (NSArray *) json {
    NSError *error;
    NSData *myDataAsJson = [NSJSONSerialization dataWithJSONObject:json
                                                           options:NSJSONWritingSortedKeys
                                                             error:&error];
    
    if (!error)
    {
        NSString *jsonString = [[NSString alloc] initWithData:myDataAsJson encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    NSLog(@"❌ERROR : Error parsing JSON to String: %@", error);
    return nil;
}

- (NSArray *) parseStringtoJSONArray : (NSString *) value {
    NSData *jsonData = [value dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (!error) {
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            return (NSArray *)jsonObject;
        }
    }
    NSLog(@"❌ERROR : Error parsing String to JSON: %@", error);
    return nil;
}

-(NSArray *)loadData : (NSString *) query
         forDatabase : (NSArray *) arrDB{
    // Get the results.
    if ([arrDB isEqual: self.arrLevel] && self.arrLevel != nil) {
        self.arrLevel = nil;
    }
    else if ([arrDB isEqual: self.arrArtwork] && self.arrArtwork != nil) {
        self.arrArtwork = nil;
    }
    else if ([arrDB isEqual: self.arrSugg] && self.arrSugg != nil) {
        self.arrSugg = nil;
    }
    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

-(int)verifyBeaconInDB : (NSArray *) arrDB forBeacon : (NSString *) beaconID{
    int indexRowInt= 0;
    
    for(NSArray* indexRow in arrDB)
    {
        if([indexRow[1] isEqualToString:beaconID])
        {
            NSLog(@"ℹ️INFORMATION : The beacon has already been read. Index : %d", indexRowInt);
            return indexRowInt;
        }
        indexRowInt++;
    }
    
    NSLog(@"ℹ️INFORMATION : The beacon has never been read");
    return -1;
}

- (void) updateView {
    switch(self.estimoteStatus)
    {
        case (StatusResearching):
            self.viewDescription.hidden = YES;
            self.viewArtwork.hidden = YES;
            self.viewStatus.hidden = NO;
            self.tvStatus.text = [NSString stringWithFormat: NSLocalizedString(@"StateResearching", nil)];
            [self.aivStatus startAnimating];
            break;
        case (StatusConnecting):
            self.tvStatus.text = [NSString stringWithFormat: NSLocalizedString(@"StateConnecting", nil)];
            break;
        case (StatusConnected):
            self.tvStatus.text = [NSString stringWithFormat: NSLocalizedString(@"StateReading", nil)];
            break;
        case (StatusRead):
            self.viewDescription.hidden = NO;
            self.viewArtwork.hidden = NO;
            self.viewStatus.hidden = YES;
            [self.aivStatus stopAnimating];
            break;
        default:
            break;
    }
}

- (IBAction)btnLocation:(UIButton *)sender {
    [self.deviceManager stopDeviceDiscovery];
    [self performSegueWithIdentifier:@"goIndoor" sender:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"CellSugg";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0];
    cell.textLabel.text = self.arrSugg[indexPath.row][2];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrSugg count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.artworkSuggName = self.arrSugg[indexPath.row][2];
    self.artworkSuggLatitude = [self.arrSugg[indexPath.row][3] floatValue];
    self.artworkSuggLongitude = [self.arrSugg[indexPath.row][4] floatValue];
    self.artworkSuggMap = self.arrSugg[indexPath.row][5];
    
    [self.deviceManager stopDeviceDiscovery];
    [self performSegueWithIdentifier:@"goIndoor2" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goIndoor"]) {
        IndoorController *destinationVC = (IndoorController *)segue.destinationViewController;
        destinationVC.nameArtwork = self.nameLevel;
        destinationVC.mapArtwork = self.mapLevel;
        destinationVC.latitudeArtwork = self.latitudeArtwork;
        destinationVC.longitudeArtwork = self.longitudeArtwork;
    }
    else if ([segue.identifier isEqualToString:@"goIndoor2"]) {
        IndoorController *destinationVC = (IndoorController *)segue.destinationViewController;
        destinationVC.nameArtwork = self.artworkSuggName;
        destinationVC.mapArtwork = self.artworkSuggMap;
        destinationVC.latitudeArtwork = self.artworkSuggLatitude;
        destinationVC.longitudeArtwork = self.artworkSuggLongitude;
    }
}

@end
