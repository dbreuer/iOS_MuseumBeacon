//
//  ManualController.h
//  MuseeSainteCroix
//
//  Created by local192 on 07/06/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManualController : UIPageViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;

@end
