//
//  EstimoteStatus.h
//  MuseeSainteCroix
//
//  Created by local192 on 31/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#ifndef EstimoteStatus_h
#define EstimoteStatus_h

typedef NS_ENUM(NSInteger, EstimoteStatus) {
    StatusDisconnected,
    StatusResearching,
    StatusFound,
    StatusConnecting,
    StatusConnected,
    StatusRead
};

#endif /* EstimoteStatus_h */
