//
//  SavoirPlusController.m
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "AboutController.h"
#import "MapController.h"
#import "InfosController.h"

@interface AboutController () <ESTDeviceConnectableDelegate, ESTDeviceManagerDelegate, ESTEddystoneManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *viewDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;

@property (weak, nonatomic) IBOutlet UIView *viewArtwork;
@property (weak, nonatomic) IBOutlet UILabel *lbArtworks;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scFilterArtwork;
@property (weak, nonatomic) IBOutlet UITableView *tvListArtwork;

- (IBAction)scFilterArtwork:(UISegmentedControl *)sender;
- (IBAction)btnLocation:(UIButton *)sender;

@end


@implementation AboutController

/*==========================================*/
/*==== Methods Configuration Controller ====*/
/*==========================================*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonBack", nil)]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    
    [self.lbDescription setText:[NSString stringWithFormat: NSLocalizedString(@"LabelMuseumDescription", nil)]];
    [self.btnMap setTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonMuseumMap", nil)] forState:UIControlStateNormal];
    [self.lbArtworks setText:[NSString stringWithFormat: NSLocalizedString(@"LabelMuseumArtworks", nil)]];
    [self.scFilterArtwork setTitle:[NSString stringWithFormat: NSLocalizedString(@"FilterMuseumName", nil)] forSegmentAtIndex:0];
    [self.scFilterArtwork setTitle:[NSString stringWithFormat: NSLocalizedString(@"FilterMuseumDate", nil)] forSegmentAtIndex:1];
    
    NSLog(@"✅SUCCESS : Welcome to A Propos!");
    
    [self configureTools];

    self.estimoteStatus = StatusDisconnected;
    [self updateStateView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateMuseum];
    [self displayInformations];
}

- (void)configureTools {
    NSLog(@"✴️STATE : Initializing Museum's TableView.....");
    self.tvListArtwork.delegate = self;
    self.tvListArtwork.dataSource = self;
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"Multi_Museums.db"];
    
    self.languageSystem = [[NSLocale preferredLanguages] firstObject];
    NSLog(@"Language System : %@", self.languageSystem);
}

- (void) updateMuseum {
    NSString *query = [[@"SELECT * FROM Museum, Address WHERE Museum.BeaconID = '" stringByAppendingString:self.beaconIDMuseum] stringByAppendingString:@"'"];
    self.arrMuseums = [self loadMuseum:query];
    NSLog(@"ℹ️INFORMATION : %lu museum(s) found", [self.arrMuseums count]);
    NSLog(@"✴️STATE : Updating Museum's TableView.....");
    [self.tvListArtwork reloadData];
}

- (void) updateStateView {
    switch(self.estimoteStatus)
    {
        case (StatusDisconnected):
            self.viewDescription.hidden = YES;
            self.viewArtwork.hidden = YES;
            break;
        case (StatusConnecting):
            
            break;
        case (StatusConnected):
            
            break;
        case (StatusRead):
            self.viewDescription.hidden = NO;
            self.viewArtwork.hidden = NO;
            break;
        default:
            break;
    }
}

/*======================================*/
/*==== Method Display Informations ====*/
/*======================================*/

- (void) displayInformations {
    self.nameMuseum = self.arrMuseums[0][2];
    NSString *multiDescMuseum = self.arrMuseums[0][3];
    
    NSArray *parsedJSON = [self parseStringtoJSONArray:multiDescMuseum];
    for( NSDictionary * indexDescription in parsedJSON) {
        if([[[indexDescription objectForKey:@"value"] lowercaseString] isEqualToString:[self.languageSystem substringToIndex:2]]) {
            self.descMuseum = [indexDescription objectForKey:@"description"];
        }
    }
    self.title = self.nameMuseum;
    self.tvDescription.text = self.descMuseum;

    self.estimoteStatus = StatusRead;
    [self updateStateView];
    
    self.addressMuseumString = [[[[[[[[self.arrMuseums[0][10] stringByAppendingString:@"\n"]
                                     stringByAppendingString:self.arrMuseums[0][9]]
                                    stringByAppendingString:@" "]
                                   stringByAppendingString:self.arrMuseums[0][8]]
                                  stringByAppendingString:@"\n"]
                                 stringByAppendingString:self.arrMuseums[0][7]]
                                stringByAppendingString:@", "]
                               stringByAppendingString:self.arrMuseums[0][6]];
    self.latitudeMuseum = [self.arrMuseums[0][11] floatValue];
    self.longitudeMuseum = [self.arrMuseums[0][12] floatValue];
    
    NSString *query = [[@"select Artwork.BeaconID, Artwork.Name from Artwork, Level, Museum where Artwork.beaconIDLevel = Level.BeaconID AND Level.BeaconIDMuseum = '" stringByAppendingString:self.beaconIDMuseum] stringByAppendingString: @"' order by Artwork.Name"];
    self.arrArtwork = [self.dbManager loadDataFromDB:query];
    [self.tvListArtwork reloadData];
}

- (NSArray *) parseStringtoJSONArray : (NSString *) value {
    NSData *jsonData = [value dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (!error) {
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            return (NSArray *)jsonObject;
        }
    }
    NSLog(@"❌ERROR : Error parsing String to JSON: %@", error);
    return nil;
}

/*==========================*/
/*==== Methods Database ====*/
/*==========================*/

-(NSArray *)loadMuseum : (NSString *) query{
    // Get the results.
    if (self.arrMuseums != nil) {
        self.arrMuseums = nil;
    }
    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

- (IBAction)scFilterArtwork:(UISegmentedControl *)sender {
    NSString *query;
    if (self.scFilterArtwork.selectedSegmentIndex == 0) {
        NSLog(@"ℹ️INFORMATION : Artworks order by name!");
        query = [[@"select Artwork.BeaconID, Artwork.Name from Artwork, Level, Museum where Artwork.beaconIDLevel = Level.BeaconID AND Level.BeaconIDMuseum = '" stringByAppendingString:self.beaconIDMuseum] stringByAppendingString: @"' order by Artwork.Name"];
        self.arrArtwork = [self.dbManager loadDataFromDB:query];
    } else if(self.scFilterArtwork.selectedSegmentIndex == 1) {
        NSLog(@"ℹ️INFORMATION : Artworks order by date!");
        query = [[@"select Artwork.BeaconID, Artwork.Name from Artwork, Level, Museum where Artwork.beaconIDLevel = Level.BeaconID AND Level.BeaconIDMuseum = '" stringByAppendingString:self.beaconIDMuseum] stringByAppendingString: @"' order by Artwork.Date"];
        self.arrArtwork = [self.dbManager loadDataFromDB:query];
    }
    [self.tvListArtwork reloadData];
}

/*===========================*/
/*==== Methods TableView ====*/
/*===========================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrArtwork count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CellArt";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    cell.textLabel.textColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0];
    cell.textLabel.text = self.arrArtwork[indexPath.row][1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.beaconIDArtwork = self.arrArtwork[indexPath.row][0];
    [self performSegueWithIdentifier:@"goInfos" sender:self];
}

/*====================*/
/*==== Navigation ====*/
/*====================*/

- (IBAction)btnLocation:(UIButton *)sender {
    [self performSegueWithIdentifier:@"goMap" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goMap"]) {
        MapController *destinationVC = (MapController *)segue.destinationViewController;
        destinationVC.name = self.nameMuseum;
        destinationVC.address = self.addressMuseumString;
        destinationVC.latitude = self.latitudeMuseum;
        destinationVC.longitude = self.longitudeMuseum;
    }
    else if ([segue.identifier isEqualToString:@"goInfos"]) {
        InfosController *destinationVC = (InfosController *)segue.destinationViewController;
        destinationVC.currentBeaconID = self.beaconIDArtwork;
        destinationVC.infosMuseum = YES;
    }
}
@end
