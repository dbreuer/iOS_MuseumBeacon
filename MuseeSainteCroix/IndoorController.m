//
//  IndoorController.m
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "IndoorController.h"

@interface IndoorController ()
@property (weak, nonatomic) IBOutlet UIImageView *ivIndoor;

@end


@implementation IndoorController

/*==========================================*/
/*==== Methods Configuration Controller ====*/
/*==========================================*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.nameArtwork;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat: NSLocalizedString(@"ButtonBack", nil)]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    // Do any additional setup after loading the view.
    
    NSLog(@"%@, %@, %f, %f", self.nameArtwork, self.mapArtwork, self.latitudeArtwork, self.longitudeArtwork);
    
    UIImage *imgPlan = [[UIImage alloc] init];
    imgPlan = [UIImage imageNamed:self.mapArtwork];
    
    UIImage *imgBeacon = [[UIImage alloc] init];
    imgBeacon = [UIImage imageNamed:@"beaconEstimote.png"];
    
    self.ivIndoor.contentMode = UIViewContentModeScaleAspectFit;
    
    UIGraphicsBeginImageContextWithOptions(imgPlan.size, FALSE, 0.0);
    [imgPlan drawInRect:CGRectMake( 0, 0, imgPlan.size.width, imgPlan.size.height)];
    [imgBeacon drawInRect:CGRectMake( (imgPlan.size.width*(self.latitudeArtwork/100))-25, (imgPlan.size.height*(self.longitudeArtwork/100))-25, imgBeacon.size.width, imgBeacon.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.ivIndoor.image = newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
