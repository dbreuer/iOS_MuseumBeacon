//
//  MenuController.h
//  MuseeSainteCroix
//
//  Created by local192 on 30/05/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "DBManager.h"
#import "EstimoteStatus.h"

@interface MenuController : UIViewController

@property (nonatomic) NSString * beaconIDMuseum;

@end
