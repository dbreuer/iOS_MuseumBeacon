//
//  ManualController.m
//  MuseeSainteCroix
//
//  Created by local192 on 07/06/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import "ManualController.h"
#import "PageController.h"

@interface ManualController ()

@end

@implementation ManualController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    PageController *initialViewController = [self viewControllerAtIndex:0];
    [initialViewController.lbManual setText:[NSString stringWithFormat: NSLocalizedString(@"TitleManual0", nil)]];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    PageController * pageController = (PageController *) viewController;
    NSUInteger index = [pageController index];
    
    if (index == 0) {
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    PageController * pageController = (PageController *) viewController;
    NSUInteger index = [pageController index];
    
    index++;
    if (index == 4) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (PageController *)viewControllerAtIndex:(NSUInteger)index {
    
    PageController *childViewController = [[PageController alloc] initWithNibName:@"PageController" bundle:nil];
    childViewController.index = index;
    
    return childViewController;
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 4;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

@end
