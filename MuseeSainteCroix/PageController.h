//
//  PageController.h
//  MuseeSainteCroix
//
//  Created by local192 on 07/06/2018.
//  Copyright © 2018 local192. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UILabel *lbManual;
@property (weak, nonatomic) IBOutlet UITextView *tvManual;

@end
